﻿using System.Collections;

namespace OOPS_Project
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //// declaration
            //Student student;
            //// initialization
            //student = new Student();
            //Student student = new Student();
            // new keyword will do 2 things
            // 1. It allocates Memory in heap for student object
            // 2. It calls constructor(def) that is used to initialize all
            // variables with their default values

            //student.GetDetails();
            // student.DisplayDetails();
            // Student student1= new Student();
            // student1.GetDetails();
            // student1.DisplayDetails();

            // Student student2= new Student();
            // student2.GetDetails();
            
            // student2.DisplayDetails();
            
            // Array of Objects
            //Student[] students= new Student[10];

            //for(int i = 0; i < 3; i++)
            //{
            //    students[i]= new Student();
            //    Console.WriteLine($"Enter Details of student no {i+1}");
            // Use Collection

            //ArrayList list = new ArrayList();
            //string ch = "y";
            //while(ch=="y")
            //{
            //    Student student = new Student();
            //    student.GetDetails();

            //    list.Add(student);
            //    Console.WriteLine("Add more ?");
            //    ch = Console.ReadLine();
            //}
            //list.Add(1);
            //foreach(Student student in list)
            //{
            //    student.DisplayDetails();
            //}

            List<Student> students = new List<Student>();
            string ch = "y";
            while (ch == "y")
            {
                Student student = new Student();
                student.GetDetails();

                students.Add(student);
                Console.WriteLine("Add more ?");
                ch = Console.ReadLine();
            }
            //list.Add(1);

            Console.WriteLine("COMAPNY IS " + Student.companyName);
            //Console.WriteLine("BAtch is " + Student.batch);
            Student.BatchDetails();
            foreach (Student student in students)
            {
                student.DisplayDetails();
            }

           
        }
    }
}